# Codi Exercises

## Random

- [Git & Github](https://gitlab.com/coditech/exercises/git_github) <kbd>🔑x5</kbd>
- [Open Source Contribution basics](https://gitlab.com/coditech/exercises/OS-contrib-basics/) <kbd>🔑x5</kbd>
- [Social Media Networking](https://gitlab.com/coditech/exercises/social-media-networking) <kbd>🔑x60 ~ 🔑x70</kbd>

## CSS

- [CV Styling](https://gitlab.com/coditech/exercises/cv-styling): create two CVs from one same HTML structure, by changing the CSS only <kbd>🔑x5 ~ 🔑x8</kbd>

## DOM & Front-End

- [Simple Login Form](https://gitlab.com/coditech/exercises/simple-login-form) <kbd>🔑x6 ~ 🔑x14</kbd>
- [Basics 01](https://gitlab.com/coditech/exercises/javascript-basics-01): variables, loops, conditionals <kbd>🔑x6</kbd>
- [Basics 02](https://gitlab.com/coditech/exercises/javascript-basics-02): DOM <kbd>🔑x8</kbd>
- [Basics 03](https://gitlab.com/coditech/exercises/javascript-basics-03): style manipulation <kbd>🔑x5</kbd>
- [Basics 04](https://gitlab.com/coditech/exercises/javascript-basics-04-tdd): Test Driven Development <kbd>🔑x2 ~ 🔑x5</kbd>
- Basics 05: *missing*
- [Basics 06](https://gitlab.com/coditech/exercises/javascript-basics-06-node-crud): CRUD <kbd>🔑x18 ~ 🔑x31</kbd>

## If you want to clone the repo

- `git clone`
- `git submodule update --init --recursive`

To update all modules to the latest version: `git submodule update --recursive --remote`